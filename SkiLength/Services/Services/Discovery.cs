﻿using Microsoft.Extensions.DependencyInjection;
using Models.Interfaces;

namespace Services.Services
{
    public static class Discovery
    {
        public static void RegisterServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ISkiLengthCalculator, SkiLengthCalculator>();
        }
    }
}
