﻿using System;
using Models.Domain;
using Models.Interfaces;

namespace Services.Services
{
    public class SkiLengthCalculator : ISkiLengthCalculator
    {
        public SkiLengthResponse CalculateSkiLength(SkiLengthRequest request)
        {
            var result = new SkiLengthResponse();

            if (request.Age < 0 || request.LengthInCm < 0 || (request.Type != SkiType.Classic && request.Type != SkiType.FreeStyle))
            {
                throw new InvalidOperationException("Bad input");
            }
            if (request.Age < 4)
            {
                result.MinLengthCm = result.MaxLengthCm = request.LengthInCm;
            }
            else if (request.Age < 8)
            {
                result.MinLengthCm = request.LengthInCm + 10;
                result.MaxLengthCm = request.LengthInCm + 20;
            }
            else if (request.Type == SkiType.Classic)
            {
                result.MaxLengthCm = result.MinLengthCm = request.LengthInCm + 20;
            }
            else if (request.Type == SkiType.FreeStyle)
            {
                result.MinLengthCm = request.LengthInCm + 10;
                result.MaxLengthCm = request.LengthInCm + 15;
            }

            if (request.Type == SkiType.Classic)
            {
                CapLengthTo207Cm(result);
            }

            return result;
        }

        internal static void CapLengthTo207Cm(SkiLengthResponse result)
        {
            result.MaxLengthCm = Math.Min(207, result.MaxLengthCm);
            result.MinLengthCm = Math.Min(207, result.MinLengthCm);
        }
    }
}
