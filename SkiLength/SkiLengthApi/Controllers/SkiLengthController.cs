﻿using Microsoft.AspNetCore.Mvc;
using Models.Interfaces;
using SkiLengthApi.Domain;

namespace SkiLengthApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SkiLengthController : ControllerBase
    {
        private readonly ISkiLengthCalculator calculator;

        public SkiLengthController(ISkiLengthCalculator calculator)
        {
            this.calculator = calculator;
        }

        [HttpGet]
        public ActionResult<SkiLengthResponseDto> Get([FromQuery] double age, [FromQuery] double lengthInCm, [FromQuery] string type)
        {
            var request = Mapper.MapToSkiLengthRequest(age, lengthInCm, type);

            if (request == null)
            {
                return BadRequest();
            }
            else
            {
                return Mapper.MapToSkiLengthResponseDto(this.calculator.CalculateSkiLength(request));
            }
        }
    }
}
