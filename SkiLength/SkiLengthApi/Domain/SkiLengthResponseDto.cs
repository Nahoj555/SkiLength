﻿namespace SkiLengthApi.Domain
{
    public class SkiLengthResponseDto
    {
        public double? MinLengthCm { get; set; }

        public double MaxLengthCm { get; set; }
    }
}
