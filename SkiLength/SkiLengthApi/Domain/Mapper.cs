﻿using System;
using Models.Domain;

namespace SkiLengthApi.Domain
{
    public static class Mapper
    {
        public static SkiLengthRequest MapToSkiLengthRequest(double age, double lengthInCm, string type)
        {
            // Typically automatically generated with AutoMapper or similar.
            if (age < 0 || lengthInCm < 0 || !Enum.TryParse(type, true, out SkiType skiType))
            {
                return null;
            }

            return new SkiLengthRequest
            {
                Age = age,
                LengthInCm = lengthInCm,
                Type = skiType
            };
        }

        public static SkiLengthResponseDto MapToSkiLengthResponseDto(SkiLengthResponse skiLengthResponse)
        {
            // Typically automatically generated with AutoMapper or similar.
            return new SkiLengthResponseDto
            {
                MaxLengthCm = skiLengthResponse.MaxLengthCm,
                MinLengthCm = skiLengthResponse.MinLengthCm
            };
        }
    }
}
