﻿using Models.Domain;

namespace Models.Interfaces
{
    public interface ISkiLengthCalculator
    {
        SkiLengthResponse CalculateSkiLength(SkiLengthRequest request);
    }
}
