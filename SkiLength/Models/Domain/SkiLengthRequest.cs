﻿namespace Models.Domain
{
    public class SkiLengthRequest
    {
        public double Age { get; set; }

        public double LengthInCm { get; set; }

        public SkiType Type { get; set; }
    }
}
