﻿namespace Models.Domain
{
    public enum SkiType
    {
        Classic = 1,

        FreeStyle = 2
    };
}
