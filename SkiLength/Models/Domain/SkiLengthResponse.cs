﻿namespace Models.Domain
{
    public class SkiLengthResponse
    {
        public double MinLengthCm { get; set; }

        public double MaxLengthCm { get; set; }
    }
}
