﻿using Microsoft.AspNetCore.Mvc;
using Models.Domain;
using Models.Interfaces;
using NSubstitute;
using NUnit.Framework;
using SkiLengthApi.Controllers;

namespace SkiLengthApi.Test
{
    class SkiLengthControllerTest
    {
        private ISkiLengthCalculator skiLengthCalculator;

        private SkiLengthController controller;

        private SkiLengthResponse skiLengthResponse;

        [SetUp]
        public void Setup()
        {
            // Typically would use framework to generate random objects.
            this.skiLengthResponse = new SkiLengthResponse
            {
                MaxLengthCm = 321,
                MinLengthCm = 123
            };

            this.skiLengthCalculator = Substitute.For<ISkiLengthCalculator>();
            this.skiLengthCalculator.CalculateSkiLength(Arg.Any<SkiLengthRequest>()).Returns(_ => this.skiLengthResponse);

            this.controller = new SkiLengthController(this.skiLengthCalculator);
        }

        [Test]
        public void TestRequest()
        {
            var response = this.controller.Get(age: 10, lengthInCm: 120, type: "classic");

            Assert.That(response.Result, Is.Null);
            Assert.That(response.Value.MaxLengthCm, Is.EqualTo(321));
            Assert.That(response.Value.MinLengthCm, Is.EqualTo(123));

            // ReSharper disable  CompareOfFloatsByEqualityOperator comparison is fine here.
            this.skiLengthCalculator.Received(1).CalculateSkiLength(Arg.Is<SkiLengthRequest>(req => req.Age == 10 && req.LengthInCm == 120 && req.Type == SkiType.Classic));
        }

        [Test]
        public void TestBadRequest()
        {
            var response = this.controller.Get(age: -1, lengthInCm: 120, type: "classic");

            Assert.That(response.Result, Is.TypeOf<BadRequestResult>());
            Assert.That(response.Value, Is.Null);

            this.skiLengthCalculator.Received(0).CalculateSkiLength(Arg.Any<SkiLengthRequest>());
        }
    }
}
