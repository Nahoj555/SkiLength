﻿using System;
using Models.Domain;
using NUnit.Framework;
using Services.Services;

namespace Services.Test
{
    [TestFixture]
    class SkiLengthCalculatorTest
    {
        private SkiLengthCalculator skiLengthCalculator;

        private Random random;

        private SkiLengthRequest request;

        [OneTimeSetUp]
        public void OnetimeSetup()
        {
            this.random = new Random();
        }

        [SetUp]
        public void Setup()
        {
            this.skiLengthCalculator = new SkiLengthCalculator();

            this.request = new SkiLengthRequest
            {
                Age = random.NextDouble() * 120,
                LengthInCm = random.NextDouble() * 190 + 50,
                Type = (SkiType)random.Next(0, 100)
            };
        }

        [Test]
        public void TestCalculateSkiLength3YFreestyle()
        {
            this.request.Age = 3;
            this.request.Type = SkiType.FreeStyle;

            this.PerformTest(this.request.LengthInCm, this.request.LengthInCm);
        }

        [Test]
        public void TestCalculateSkiLength7YFreestyle()
        {
            this.request.Age = 7;
            this.request.Type = SkiType.FreeStyle;

            this.PerformTest(this.request.LengthInCm + 10, this.request.LengthInCm + 20);
        }

        [Test]
        public void TestCalculateSkiLength25YFreestyle()
        {
            this.request.Age = 25;
            this.request.Type = SkiType.FreeStyle;

            this.PerformTest(this.request.LengthInCm + 10, this.request.LengthInCm + 15);
        }

        [Test]
        public void TestCalculateSkiLength3YClassic()
        {
            this.request.Age = 3;
            this.request.Type = SkiType.Classic;
            this.request.LengthInCm = 70;

            this.PerformTest(this.request.LengthInCm, this.request.LengthInCm);
        }

        [Test]
        public void TestCalculateSkiLength7YClassic()
        {
            this.request.Age = 7;
            this.request.Type = SkiType.Classic;
            this.request.LengthInCm = 110;

            this.PerformTest(this.request.LengthInCm + 10, this.request.LengthInCm + 20);
        }

        [Test]
        public void TestCalculateSkiLength25YClassic()
        {
            this.request.Age = 25;
            this.request.Type = SkiType.Classic;
            this.request.LengthInCm = 180;

            this.PerformTest(this.request.LengthInCm + 20, this.request.LengthInCm + 20);
        }

        [Test]
        public void TestVerifyFreestyleLong()
        {
            this.request.LengthInCm = 300;
            this.request.Type = SkiType.FreeStyle;

            var result = this.skiLengthCalculator.CalculateSkiLength(this.request);

            Assert.That(result.MaxLengthCm, Is.GreaterThanOrEqualTo(300));
            Assert.That(result.MinLengthCm, Is.GreaterThanOrEqualTo(300));
        }

        [Test]
        public void TestVerifyClassicLong()
        {
            this.request.LengthInCm = 300;
            this.request.Type = SkiType.Classic;

            this.PerformTest(207, 207);
        }

        [Test]
        public void TestCalculateSkiLengthNegativeAge()
        {
            this.request.Age = -12;

            Assert.That(() => this.skiLengthCalculator.CalculateSkiLength(this.request), Throws.InvalidOperationException);
        }

        [Test]
        public void TestCalculateSkiLengthNegativeLength()
        {
            this.request.LengthInCm = -3;

            Assert.That(() => this.skiLengthCalculator.CalculateSkiLength(this.request), Throws.InvalidOperationException);
        }

        [Test]
        public void TestCalculateSkiLengthBadSkiType()
        {
            this.request.Type = (SkiType)999;

            Assert.That(() => this.skiLengthCalculator.CalculateSkiLength(this.request), Throws.InvalidOperationException);
        }

        public void PerformTest(double expectedMinLengthInCm, double expectedMaxLengthInCm)
        {
            var result = this.skiLengthCalculator.CalculateSkiLength(this.request);

            Assert.That(result.MinLengthCm, Is.EqualTo(expectedMinLengthInCm));
            Assert.That(result.MaxLengthCm, Is.EqualTo(expectedMaxLengthInCm));
        }
    }
}
